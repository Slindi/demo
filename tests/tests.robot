*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
Test Firefox1
    Open Browser   http://35.205.246.172:8080/    Firefox
    Title Should Be     Hello World
    Page Should Contain     World
    Close Browser

Test Firefox2
    Open Browser   http://35.205.246.172:8080/    Firefox
    Page Should Contain     World
    Close Browser

Test Chrome
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome      chrome_options=${chrome_options}
    Go To   http://35.205.246.172:8080/
    Title Should Be     Hello World
    Close Browser
